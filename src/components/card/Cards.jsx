import s from './Cards.module.scss';
import cn from 'classnames';
import { ReactComponent as StarIcon } from '../../assets/star.svg';
import { ReactComponent as StarIconOn } from '../../assets/starOn.svg';
import PropTypes from 'prop-types';

function Cards (props) {
        const { onClick, setFav, cards } = props;

        const favs = localStorage.getItem(2);

        const liCards = card => (
            <li key={card.art} className={s.card}>
                <img className={s.cardImg} src={card.picUrl} alt='picture' />
                <div className={s.title}>{card.title}</div>
                <div className='stars' onClick={() => setFav(card)}>
                    {favs && localStorage.getItem(2).indexOf(card.art) !== -1 ? (
                        <>
                            <StarIconOn />
                            <StarIconOn />
                            <StarIconOn />
                            <StarIconOn />
                            <StarIconOn />
                        </>
                    ) : (
                        <>
                            <StarIcon />
                            <StarIcon />
                            <StarIcon />
                            <StarIcon />
                            <StarIcon />
                        </>
                    )}
                </div>
                <div className={s.textCard}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus quo eligendi quam optio.</div>
                <div className={s.price_to_cart}>
                    <p className={s.price}>{card.price}</p>
                    <button className={s.toCart} onClick={() => onClick(card, true)}>
                        TO CART
                    </button>
                </div>
            </li>
        );

        return cards.map(card => liCards(card));
}

export default Cards;

Cards.protoType = {
    onClick: PropTypes.func.isRequired,
    setFav: PropTypes.func.isRequired,
    cards: PropTypes.object.isRequired,
};
