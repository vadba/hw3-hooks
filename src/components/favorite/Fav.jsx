import { Component } from 'react';
import s from './Fav.module.scss';
import cn from 'classnames';
import PropTypes from 'prop-types';
import Cart from '../cart';
import { ReactComponent as StarIconOn } from '../../assets/starOn.svg';

function Fav (props) {

        const { favs, setFavs } = props;

        const deleteFav = (e) => {
          const event = e.target.id || e.target.parentElement.parentElement.parentElement.id;
          setFavs((prevState) => [ ...prevState.filter(cart => cart.art !== event)]);
          localStorage.setItem(2, localStorage.getItem(2).split(',').filter(str => str !== event));
          if (localStorage.getItem(2) === '') {
            localStorage.removeItem(2);
          }
        }

        const renderFavs = fav => (
            <li key={fav.art} className={s.cart}>
              <span className={s.starIconOn} id={fav.art} onClick={deleteFav}><StarIconOn /></span>
                <img className={s.cartImg} src={fav.picUrl} alt='picture' />
                <div className={s.title}>{fav.title}</div>
                <p className={s.price}>{fav.price}</p>
            </li>
        );


        return favs.map(fav => renderFavs(fav));

}

export default Fav;

Cart.protoType = {
  carts: PropTypes.object.isRequired,
};
