import { Component } from 'react';
import s from './Cart.module.scss';
import cn from 'classnames';
import PropTypes from 'prop-types';

function Cart (props) {

        const { carts, setCarts, deleteCart } = props;

        // const deleteCart = (e) => {
        //   setCarts((prevState) => [ ...prevState.filter(cart => cart.art !== e.target.id)]);
        //   localStorage.setItem(1, localStorage.getItem(1).split(',').filter(str => str !== e.target.id));
        //   if (localStorage.getItem(1) === '') {
        //     localStorage.removeItem(1);
        //   }
        // }

        const renderCards = cart => (
            <li key={cart.art} className={s.cart}>
              <span className={s.deleteItem} id={cart.art} onClick={() => deleteCart(cart.art, false)}>&times;</span>
                <img className={s.cartImg} src={cart.picUrl} alt='picture' />
                <div className={s.title}>{cart.title}</div>
                <p className={s.price}>{cart.price}</p>
            </li>
        );

        return carts.map(cart => renderCards(cart));

}

export default Cart;

Cart.protoType = {
  carts: PropTypes.object.isRequired,
};
