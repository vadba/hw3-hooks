import React from "react";
import { Route, Routes } from 'react-router-dom';
import Cards from '../components/card/Cards';
import Cart from '../components/cart';
import Fav from '../components/favorite';

const Router = (props) => {

  const { onClick, setFav, cards, carts, favs, setCarts, setFavs, deleteCart} = props;

  return (
    <Routes>
        <Route
          path="/"
          exact
          element={<Cards onClick={onClick} setFav={setFav} cards={cards}/>} />
        <Route
          path="/cart"
          element={
            <div>
              <Cart carts={carts} setCarts={setCarts}       deleteCart={deleteCart}
                    />
            </div>
          }
        />
        <Route
          path="/favorites"
          element={
            <div>
              <Fav favs={favs} setFavs={setFavs}/>
            </div>
          }
        />

    </Routes>
  );
};

export default Router;
