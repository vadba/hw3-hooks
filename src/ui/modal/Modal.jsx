import { Component } from 'react';
import s from './Modal.module.scss';
import cn from 'classnames';
import PropTypes from 'prop-types';
import Cart from '../../components/cart';

class Modal extends Component {
    render() {
        const {
            opened,
            onClose,
            header = 'Do you want to delete?',
            closeButton = false,
            text = 'Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete it?',
            children,
            backgroundColor,
            backgroundColorTitle,
        } = this.props;

        return (
            <>
                {opened && (
                    <>
                        <div className={s.overlay} onClick={onClose}></div>
                        <div className={cn(s.openModal, s[backgroundColor])}>
                            {closeButton && (
                                <button className={s.closeButton} onClick={onClose}>
                                    &times;
                                </button>
                            )}
                            <h4 className={cn(s.modalTitle, s[backgroundColorTitle])}>{header}</h4>
                            <div className={s.modalContent}>
                                <p className={s.modalText}>{text}</p>
                                <div className={s.buttons}>{children}</div>
                            </div>
                        </div>
                    </>
                )}
            </>
        );
    }
}

export default Modal;

Modal.protoType = {
  opened: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  header: PropTypes.string.isRequired,
  closeButton: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired,
  backgroundColorTitle: PropTypes.string.isRequired,
};
