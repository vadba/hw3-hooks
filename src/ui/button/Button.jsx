import { Component } from 'react';
import s from './Button.module.scss';
import cn from 'classnames';
import PropTypes from 'prop-types';
import Modal from '../modal';

class Button extends Component {
    render() {
        const { onClick, className, children, text, backgroundColor } = this.props;

        return (
            <button onClick={onClick} className={cn(s[className], s.button, s[backgroundColor])}>
                {text || children}
            </button>
        );
    }
}

export default Button;

Button.protoType = {
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired,
};
