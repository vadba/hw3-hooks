import { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.scss';
import Button from './ui/button/Button';
import Modal from './ui/modal/Modal';
import List from './components/list/List';
import { request } from './tools/request';
import { ReactComponent as CartIcon } from './assets/cart.svg';
import { ReactComponent as FavIcon } from './assets/fav.svg';
import { Link, NavLink } from "react-router-dom";
import Router from './routes/router';

function App () {
  const [openedModal2, setModal] = useState(false);
  const [editModalData, setEMData] = useState({});
  const [products, setProducts] = useState([]);
  const [carts, setCarts] = useState([]);
  const [favs, setFavs] = useState([]);
  const [func, setFunc] = useState(true);
  const [art, setArt] = useState();

    const getProducts = async () => {
        const { res, err } = await request();
        if (res) {
            return setProducts(res);
        }

        if (err) {
            return console.log(err);
        }
    };

    const handleOpenModal2 = (data, flag) => {
        setModal( true );
        if(flag) {
        setEMData( data );
        }else {
          setFunc(false);
          setArt(data);
        }
    };

    const handleCloseModal2 = () => {
      setModal(false );
    };

    useEffect(() => {
        localStorage.removeItem(1);
        localStorage.removeItem(2);
        getProducts();
    }, [])


    function setCardId () {
        if (localStorage.getItem(1) !== null) {
            if (!localStorage.getItem(1).indexOf(editModalData.art)) {
                return setModal(false );
            }
            setCarts((prevState) => [ ...prevState,editModalData]);
            const newStoreCart = localStorage.getItem(1) + ',' + editModalData.art;

            localStorage.setItem(1, newStoreCart);
        } else {
            setCarts((prevState) => [ ...prevState, editModalData]);
            localStorage.setItem(1, editModalData.art);
        }
        setModal(false );
    };

    function deleteCart () {
      if (localStorage.getItem(1)) {
        setCarts((prevState) => [ ...prevState.filter(cart => cart.art !== art)]);
        localStorage.setItem(1, localStorage.getItem(1).split(',').filter(str => str !== art));
        if (localStorage.getItem(1) === '') {
          localStorage.removeItem(1);
        }
      }
      handleCloseModal2();
    }

    const setFav = data => {
        if (localStorage.getItem(2) !== null) {
            if (!localStorage.getItem(2).indexOf(data.art)) {
                return;
            }
            setFavs( (prevState) => [...prevState, data] );
            const newFavs = localStorage.getItem(2) + ',' + data.art;

            localStorage.setItem(2, newFavs);
        } else {
          setFavs( (prevState) => [...prevState, data] );
            localStorage.setItem(2, data.art);
        }
    };


        return (
            <div className='App'>
                <Modal opened={openedModal2} onClose={handleCloseModal2} header='TO Cart' closeButton={true} text='Lorem text' backgroundColor='yellow' backgroundColorTitle='green'>
                    <Button className={'buttonfirst'} text='OK' onClick={func ? setCardId : deleteCart} backgroundColor='orange' />
                    <Button className={'buttonfirst'} text='Cancel' backgroundColor='orange' onClick={handleCloseModal2} />
                </Modal>
                <div className='App-conainer'>
                    <header className='App-header'>
                      <div className='first-header'>
                        <img src={logo} className='App-logo' alt='logo' />
                        <div className='header-reg-cart'>
                            <div className='link-register'>Login / Register</div>

                            <div className='hover-cart'>
                                {localStorage.getItem(1) ? <span className='int-cart'>{localStorage.getItem(1).split(',').length}</span> : ''}
                                <CartIcon />
                                {/*<div className='cart-list'>*/}
                                {/*    <Cart carts={carts} />*/}
                                {/*</div>*/}
                            </div>

                            <div className='hover-fav'>
                                {localStorage.getItem(2) ? <span className='int-fav'>{localStorage.getItem(2).split(',').length}</span> : ''}
                                <FavIcon />
                                {/*<div className='fav-list'>*/}
                                {/*    <Fav carts={favs} />*/}
                                {/*</div>*/}
                            </div>
                        </div>
                      </div>
                      <div className='second-header'>
                        <NavLink
                          to="/"
                          className='nav-link'
                        >
                          <h4>Продукты</h4>
                        </NavLink>
                        <NavLink
                          to="/cart"
                          className='nav-link'
                        >
                          <h4>Cart</h4>
                        </NavLink>
                        <NavLink
                          to="/favorites"
                          className='nav-link'
                        >
                          <h4>Favorites</h4>
                        </NavLink>
                      </div>
                    </header>
                    <main className='App-main'>
                        <List>
                            <Router cards={products} onClick={handleOpenModal2} setFav={setFav} carts={carts} favs={favs} setCarts={setCarts} setFavs={setFavs} deleteCart={handleOpenModal2}
                              />
                        </List>
                    </main>
                </div>
            </div>
        );

}

export default App;
